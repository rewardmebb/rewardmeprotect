package com.mollatech.service.nucleus.crypto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.x509.X509V3CertificateGenerator;

public class CertUtil {

    static {
        Provider p = Security.getProvider("BC");
        if (p == null) Security.addProvider(new BouncyCastleProvider());
    }

    private Certificate m_myCert = null;

    private PrivateKey m_myPrivateKey = null;

    private PublicKey m_myPubKey = null;

    public PrivateKey getPrivateKey() {
        return m_myPrivateKey;
    }

    public Certificate getCert() {
        return m_myCert;
    }

    public KeyPair generateKeyPair(int iStrength) throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "BC");
        kpg.initialize(iStrength);
        KeyPair kp = kpg.genKeyPair();
        return kp;
    }

    public byte[] generatePFX(String strCertAliasName, Certificate[] userCertChain, KeyPair kp, String strPIN) throws KeyStoreException, NoSuchProviderException, NoSuchAlgorithmException, CertificateException, IOException {
        byte[] m_bap12 = null;
        KeyStore store = KeyStore.getInstance("PKCS12", "BC");
        store.load(null, null);
        store.setKeyEntry(strCertAliasName, kp.getPrivate(), null, userCertChain);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        store.store(baos, strPIN.toCharArray());
        store = null;
        m_bap12 = baos.toByteArray();
        return m_bap12;
    }

    public String generateB64EncodedCSR(KeyPair kp, Vector ordering, Hashtable attributes) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {
        X509Principal subject = null;
        subject = new X509Principal(ordering, attributes);
        String strALGO = LoadSettings.g_sSettings.getProperty("certificate.generation.algorithm");
        PKCS10CertificationRequest req1 = null;
        try {
            req1 = new PKCS10CertificationRequest(strALGO, subject, kp.getPublic(), null, kp.getPrivate(), "BC");
        } catch (Exception e) {
            e.printStackTrace();
        }
        byte[] baDerCerReq = req1.getEncoded();
        return new String(Base64.encode(baDerCerReq));
    }

    public Certificate generateUserCert(Vector ordering, Hashtable attributes, PublicKey userPublicKey, PrivateKey caPrivKey, X509Certificate caCert, int iValidityDays, String strSrNo) throws InvalidKeyException, CertificateException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {
        X509Principal subject = null;
        subject = new X509Principal(ordering, attributes);
        X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();
        if (strSrNo.startsWith("AP") == false) v3CertGen.setSerialNumber(new BigInteger(strSrNo)); else {
            strSrNo = strSrNo.replaceFirst("AP", "");
            v3CertGen.setSerialNumber(new BigInteger(strSrNo));
        }
        v3CertGen.setIssuerDN(PrincipalUtil.getSubjectX509Principal(caCert));
        v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 1000L * 60));
        v3CertGen.setNotAfter(new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * iValidityDays)));
        v3CertGen.setSubjectDN(subject);
        v3CertGen.setPublicKey(userPublicKey);
        v3CertGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
        X509Certificate cert = v3CertGen.generateX509Certificate(caPrivKey);
        return cert;
    }

    public int loadPfxFromFile(String strPFXPath, String strPassword, String strCAalias) throws Exception {
        if (strPFXPath.compareTo("") == 0 || strPassword.compareTo("") == 0 || strCAalias.compareTo("") == 0) {
            return -1;
        }
        File fp12 = new File(strPFXPath);
        if (!fp12.exists()) {
            throw new Exception("PFX file path is invalid");
        }
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(fp12);
            byte[] buffer = new byte[(int) fp12.length()];
            int iRead = fin.read(buffer);
            ByteArrayInputStream p12ByteArray = new ByteArrayInputStream(buffer);
            KeyStore ks = KeyStore.getInstance("PKCS12", "BC");
            InputStream in = p12ByteArray;
            ks.load(in, strPassword.toCharArray());
            in.close();
            fin.close();
            Enumeration e = ks.aliases();
            boolean b = false;
            while (e.hasMoreElements()) {
                String alias = (String) e.nextElement();
                b = ks.isKeyEntry(alias);
                if (b == true) {
                    if (alias.equals(strCAalias) == true) {
                        Key key = ks.getKey(strCAalias, strPassword.toCharArray());
                        if (key instanceof PrivateKey) {
                            m_myCert = ks.getCertificate(strCAalias);
                            m_myPubKey = m_myCert.getPublicKey();
                            m_myPrivateKey = (PrivateKey) key;
                        }
                    } else {
                        System.out.println("DOES NOT MATCH INPUT ALIAS [" + strCAalias + "]");
                        continue;
                    }
                }
            }
            if (b == false) {
                throw new Exception("Key corresponding to Alias is not present. Please check Alias Name Or password.");
            }
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                fin.close();
            } catch (IOException ex) {
                throw ex;
            }
        }
        return 0;
    }

    public Certificate generateServerCert(Vector ordering, Hashtable attributes, PublicKey myPublicKey, PrivateKey myPrivKey, X500Principal dnName, int iValidityDays, String strSrNo) throws InvalidKeyException, CertificateException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {
        X509Principal subject = null;
        subject = new X509Principal(ordering, attributes);
        X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();
        v3CertGen.setSerialNumber(new BigInteger(strSrNo));
        v3CertGen.setIssuerDN(dnName);
        v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 1000L * 60));
        v3CertGen.setNotAfter(new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * iValidityDays)));
        v3CertGen.setSubjectDN(subject);
        v3CertGen.setPublicKey(myPublicKey);
        v3CertGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
        X509Certificate cert = v3CertGen.generateX509Certificate(myPrivKey);
        return cert;
    }

    public KeyStore convertPFX(String pkcs12File, String password, boolean saveToFile, String keyStoreType, String jksFile) throws Exception {
        FileInputStream fis = new FileInputStream(pkcs12File);
        KeyStore ks = KeyStore.getInstance("PKCS12");
        ks.load(fis, password.toCharArray());
        KeyStore jksKeyStore = KeyStore.getInstance(keyStoreType);
        jksKeyStore.load(null, null);
        Enumeration aliases = ks.aliases();
        while (aliases.hasMoreElements()) {
            String strAlias = (String) aliases.nextElement();
            System.out.println("Found alias: " + strAlias + " in PKCS12 key store");
            if (ks.isKeyEntry(strAlias)) {
                Key key = ks.getKey(strAlias, password.toCharArray());
                if (key instanceof PrivateKey) {
                    Certificate[] chain = ks.getCertificateChain(strAlias);
                    if (chain != null) {
                        jksKeyStore.setKeyEntry(strAlias, key, password.toCharArray(), chain);
                    } else {
                        System.out.println((new Date()) + ">> " + "No certificate chain was available for the private key entry with alias: " + strAlias + ". This could happen if the PFX has not been created correctly or no friendly name has been specified.");
                    }
                } else {
                    jksKeyStore.setKeyEntry(strAlias, key, password.toCharArray(), null);
                }
            }
        }
        if (saveToFile == true && jksFile != null && jksFile.isEmpty() == false) {
            OutputStream out = new FileOutputStream(jksFile);
            jksKeyStore.store(out, password.toCharArray());
            out.close();
        }
        return jksKeyStore;
    }
}
