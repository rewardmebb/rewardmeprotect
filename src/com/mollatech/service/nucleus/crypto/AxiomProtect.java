package com.mollatech.service.nucleus.crypto;

import com.license4j.HardwareID;
import com.license4j.License;
import com.license4j.LicenseValidator;
import com.mollatech.rewardme.mobiletrust.crypto.CryptoManager;
import static com.mollatech.service.nucleus.crypto.HWSignature.asHex;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AxiomProtect {

    private static String licfile = null;

    private static String publickey = "";

    private static String aeskey = null;

    private static Properties g_sSettings = null;

    private static byte[] aes_password = null;

    private static String g_filepath = null;

    private static Date g_sCheckDateForLicense = null;

    public static String Resources = "Import_Resources";

    public static String Accesspoints = "Export_Access_points";

    public static String GroupManagement = "Group_Management";

    public static String Partnermanagement = "Partner_management";

    public static String Email_Gateway_Connector = "Email_Gateway_Connector";

    public static String SMS_Gateway_Connector = "SMS_Gateway_Connector";

    public static String Import_SOAP_Web_Services = "Import_SOAP_Web_Services";

    public static String Import_HTTP_Post_Services = "Import_HTTP_Post_Services";

    public static String Import_Socket_Based_Services = "Import_Socket_Based_Services";

    public static String Export_SOAP_Web_Services = "Export_SOAP_WebServices";

    public static String Export_Restful_Web_Serivces = "Export_Restful_Web_Serivces";

    public static String Transform_API = "Transform_API";

    public static String Transform_Parameters = "Transform_Parameters";

    public static String Report_Manager = "Report_Manager";

    public static String IP_Based_Authentication = "IP_Based_Authentication";

    public static String Certificate_based_Authentication = "Certificate_based_Authentication";

    public static String Secret_Token_based_Authentication = "Secret_Token_based_Authentication";

    public static String Day_And_Time_Restriction = "Day_And_Time_Restriction";

    public static String Transactions_per_day_Controller = "Transactions_per_day_Controller";

    public static String Transaction_per_second_tps_controller = "Transaction_per_second_tps_controller";

    public static String Resource_Monitoring = "Resource_Monitoring";

    public static String Billing_manager = "Billing_manager";

    public static String Clustering_between_instances = "Clustering_between_instances";

    static {
        LoadAxiomProtect();
        ValidateLicense();
    }

    private static void LoadAxiomProtect() {
        try {
            String sep = System.getProperty("file.separator");
            String usrhome = System.getProperty("catalina.home");
            if (usrhome == null) {
                usrhome = System.getenv("catalina.home");
            }
            if (usrhome == null) {
                usrhome = System.getenv("SGHOME");
            }
            if (usrhome == null) {
                String path = sep + "root" + sep +  "TMAPIV2" + sep +  "worker";
                File f = new File(path);
                if (f.exists() && f.isDirectory()) {
                    usrhome = path;
                } else {
                    path = sep + "sguard" + sep + "worker";
                    f = new File(path);
                    if (f.exists() && f.isDirectory()) {
                        usrhome = path;
                    } else {
                        path = sep + "app" + sep + "worker";
                        f = new File(path);
                        if (f.exists() && f.isDirectory()) {
                            usrhome = path;
                        } else {
                            path = "C:\\Users\\Ash\\Desktop\\apache-tomcat-8.0.24";
                            f = new File(path);
                            if (f.exists() && f.isDirectory()) {
                                usrhome = path;
                            } else {
                                path = "/Users/Ashu/Pictures/ashish/Hackathon";
                                f = new File(path);
                                if (f.exists() && f.isDirectory()) {
                                    usrhome = path;
                                }
                            }
                        }
                    }
                }
            }
            if (usrhome == null) {
                //usrhome = "/home/axiom/admin";
                usrhome = "/Users/abhishekingle/Abhi/RewardMe/apache-tomcat-8.0.24";
            }
            usrhome += sep + "rewardme-settings";
            g_filepath = usrhome + sep + "dbsetting.conf";
            PropsFileUtil p = new PropsFileUtil();
            if (p.LoadFile(g_filepath) == true) {
                g_sSettings = p.properties;
            } else {
                System.out.println((new Date()) + ">> " + "license  file failed to load >> " + g_filepath);
            }
            aeskey = g_sSettings.getProperty("reserved.1");
            String strHashKey = g_sSettings.getProperty("reserved.7");
            publickey = g_sSettings.getProperty("reserved.2");
            File f = new File(g_sSettings.getProperty("licence.path"));
            if (f.exists()) {
                licfile = getContents(f);
            }
            if (aeskey != null && aeskey.isEmpty() == false) {
                AES aesObj = new AES();
                byte[] byteHashKey = aesObj.PINDecryptBytes(strHashKey, AES.getSignature());
                aes_password = aesObj.PINDecryptBytes(aeskey, asHex(byteHashKey));
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int SetPasswordsInner(String[] passwords) {
        try {
            if (passwords == null || passwords.length == 0) {
                return -3;
            }
            MessageDigest md = MessageDigest.getInstance("SHA1");
            for (int i = 0; i < passwords.length; i++) {
                md.update(passwords[i].getBytes());
            }
            byte[] outputHashOFCredentials = md.digest();
            md.reset();
            AES aesObj = new AES();
            String password = AES.getSignature();
            String strEncryptedKEY = aesObj.PINEncryptBytes(outputHashOFCredentials, password);
            String password2 = asHex(outputHashOFCredentials);
            byte[] dbPassword = GenerateRandomNumber().getBytes();
            String strEndDBPassword = aesObj.PINEncryptBytes(dbPassword, password2);
            PropsFileUtil pfUtilsObj = new PropsFileUtil();
            boolean bResult = false;
            bResult = pfUtilsObj.LoadFile(g_filepath);
            HashMap map = new HashMap();
            pfUtilsObj.properties.setProperty("reserved.7", strEncryptedKEY);
            pfUtilsObj.properties.setProperty("reserved.1", strEndDBPassword);
            Enumeration enamObj = pfUtilsObj.properties.propertyNames();
            while (enamObj.hasMoreElements()) {
                String key = (String) enamObj.nextElement();
                String value = (String) pfUtilsObj.properties.getProperty(key);
                map.put(key, value);
            }
            bResult = pfUtilsObj.ReplaceProperties(map, g_filepath);
            if (bResult == true) {
                LoadAxiomProtect();
                return 0;
            } else {
                return -2;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    private static int ReplacePasswordsInner(String[] curpasswords, String[] newpasswords) {
        try {
            if (curpasswords == null || curpasswords.length == 0) {
                return -4;
            }
            if (newpasswords == null || newpasswords.length == 0) {
                return -3;
            }
            MessageDigest md = MessageDigest.getInstance("SHA1");
            for (int i = 0; i < curpasswords.length; i++) {
                md.update(curpasswords[i].getBytes());
            }
            byte[] outputCurrentAESKEY = md.digest();
            PropsFileUtil pfUtilsObj = new PropsFileUtil();
            boolean bResult = pfUtilsObj.LoadFile(g_filepath);
            HashMap map = new HashMap();
            String strCurPassInHex = pfUtilsObj.properties.getProperty("reserved.7");
            AES aesObj = new AES();
            byte[] bytesDecryptedCurPassword = aesObj.PINDecryptBytes(strCurPassInHex, AES.getSignature());
            String CurPasswordFromFile = asHex(bytesDecryptedCurPassword);
            String CurPasswordFromDashbd = asHex(outputCurrentAESKEY);
            if (CurPasswordFromDashbd.compareTo(CurPasswordFromFile) != 0) {
                Date d = new Date();
                System.out.println(d + ">>" + "Failed to match");
                return -9;
            }
            md.reset();
            for (int i = 0; i < newpasswords.length; i++) {
                md.update(newpasswords[i].getBytes());
            }
            byte[] outputNewHashFromOfficers = md.digest();
            String strEncryptedKEY = aesObj.PINEncryptBytes(outputNewHashFromOfficers, AES.getSignature());
            pfUtilsObj.properties.setProperty("reserved.7", strEncryptedKEY);
            String strEncRandomKey = pfUtilsObj.properties.getProperty("reserved.1");
            byte[] strPlainRandomKey = aesObj.PINDecryptBytes(strEncRandomKey, asHex(bytesDecryptedCurPassword));
            String strEncNewRandomKey = aesObj.PINEncryptBytes(strPlainRandomKey, asHex(outputNewHashFromOfficers));
            pfUtilsObj.properties.setProperty("reserved.1", strEncNewRandomKey);
            Enumeration enamObj = pfUtilsObj.properties.propertyNames();
            while (enamObj.hasMoreElements()) {
                String key = (String) enamObj.nextElement();
                String value = (String) pfUtilsObj.properties.getProperty(key);
                map.put(key, value);
            }
            bResult = pfUtilsObj.ReplaceProperties(map, g_filepath);
            if (bResult == true) {
                LoadAxiomProtect();
                return 0;
            } else {
                return -2;
            }
        } catch (Exception ex) {
            Logger.getLogger(AxiomProtect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    private static int g_sLicenseResult = -1;

    public static int ValidateLicense() {
        try {
            License license = null;
            license = LicenseValidator.validate(licfile, publickey, null, null, null, null, null);
            int status = 0;
            switch(license.getValidationStatus()) {
                case LICENSE_VALID:
                    status = 0;
                    break;
                case LICENSE_INVALID:
                    status = -1;
                    break;
                case LICENSE_EXPIRED:
                    status = 1;
                    break;
                case LICENSE_MAINTENANCE_EXPIRED:
                    status = 2;
                    break;
                case MISMATCH_HARDWARE_ID:
                    status = 6;
                    break;
                case MISMATCH_PRODUCT_ID:
                    break;
                case MISMATCH_PRODUCT_EDITION:
                    status = 4;
                    break;
                case MISMATCH_PRODUCT_VERSION:
                    status = 5;
                    break;
            }
            return status;
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public static String GetHardwareID() {
        String strHWIDOrg = "System cannot be issued license. it is virtual without any static profile";
        String strHWIDFound = null;
        try {
            strHWIDFound = HardwareID.getHardwareIDFromHDDSerial();
            if (strHWIDFound == null || strHWIDFound.isEmpty() == true) {
                try {
                    strHWIDFound = HardwareID.getHardwareIDFromVolumeSerialNumber();
                } catch (Exception e) {
                }
                if (strHWIDFound == null || strHWIDFound.isEmpty() == true) {
                    try {
                        strHWIDFound = HardwareID.getHardwareIDFromEthernetAddress();
                    } catch (Exception e) {
                    }
                }
                if (strHWIDFound == null || strHWIDFound.isEmpty() == true) {
                    try {
                        strHWIDFound = HardwareID.getHardwareIDFromHostName();
                    } catch (Exception e) {
                    }
                }
            }
            if (strHWIDFound == null || strHWIDFound.isEmpty() == true) {
                strHWIDOrg = strHWIDFound;
            }
        } catch (Exception e) {
        }
        return strHWIDFound;
    }

    public static int CheckLicense(String LicFilePath) {
        String path = new File(LicFilePath).getPath();
        LicFilePath = getContents(new File(LicFilePath));
        try {
            License license = null;
            license = LicenseValidator.validate(LicFilePath, publickey, null, null, null, null, null);
            int status = 0;
            switch(license.getValidationStatus()) {
                case LICENSE_VALID:
                    status = 0;
                    PropsFileUtil pfUtilsObj = new PropsFileUtil();
                    boolean bResult = false;
                    pfUtilsObj.LoadFile(g_filepath);
                    HashMap map = new HashMap();
                    pfUtilsObj.properties.setProperty("licence.path", path);
                    Enumeration enamObj = pfUtilsObj.properties.propertyNames();
                    while (enamObj.hasMoreElements()) {
                        String key = (String) enamObj.nextElement();
                        String value = (String) pfUtilsObj.properties.getProperty(key);
                        map.put(key, value);
                    }
                    bResult = pfUtilsObj.ReplaceProperties(map, g_filepath);
                    if (bResult == true) {
                        LoadAxiomProtect();
                    } else {
                        return -2;
                    }
                    break;
                case LICENSE_INVALID:
                    status = -1;
                    break;
                case LICENSE_EXPIRED:
                    status = 1;
                    break;
                case LICENSE_MAINTENANCE_EXPIRED:
                    status = 2;
                    break;
                case MISMATCH_HARDWARE_ID:
                    status = 6;
                    break;
                case MISMATCH_PRODUCT_ID:
                    break;
                case MISMATCH_PRODUCT_EDITION:
                    status = 4;
                    break;
                case MISMATCH_PRODUCT_VERSION:
                    status = 5;
                    break;
            }
            return status;
        } catch (Exception e) {
            e.printStackTrace();
            return -99;
        }
    }

    public static Properties LicenseDetails() {
        try {
            License license = LicenseValidator.validate(licfile, publickey, null, null, null, null, null);
            Properties p = new Properties();
            p.setProperty("Product ID", license.getLicenseText().getLicenseProductName());
            p.setProperty("Validity Period ( in days)", "" + license.getLicenseText().getLicenseValidityPeriod());
            p.setProperty("Support Expires On", "" + license.getLicenseText().getLicenseExpireDate());
            p.setProperty("Registration Profile", license.getLicenseText().getLicenseHardwareID());
            p.setProperty("Company Name", license.getLicenseText().getUserCompany());
            p.setProperty("Email Id", license.getLicenseText().getUserEMail());
            p.setProperty("Telephone", license.getLicenseText().getUserTelephone());
            p.setProperty("License ID", license.getLicenseText().getLicenseHardwareID());
            HashMap pDetails = license.getLicenseText().getCustomSignedFeatures();
            Properties pItems = mapToProperties(pDetails);
            p.setProperty(Resources, pItems.getProperty(Resources));
            p.setProperty(Accesspoints, pItems.getProperty(Accesspoints));
            p.setProperty(GroupManagement, pItems.getProperty(GroupManagement));
            p.setProperty(Partnermanagement, pItems.getProperty(Partnermanagement));
            p.setProperty(Email_Gateway_Connector, pItems.getProperty(Email_Gateway_Connector));
            p.setProperty(SMS_Gateway_Connector, pItems.getProperty(SMS_Gateway_Connector));
            p.setProperty(Import_SOAP_Web_Services, pItems.getProperty(Import_SOAP_Web_Services));
            p.setProperty(Import_HTTP_Post_Services, pItems.getProperty(Import_HTTP_Post_Services));
            p.setProperty(Import_Socket_Based_Services, pItems.getProperty(Import_Socket_Based_Services));
            p.setProperty(Export_SOAP_Web_Services, pItems.getProperty(Export_SOAP_Web_Services));
            p.setProperty(Export_Restful_Web_Serivces, pItems.getProperty(Export_Restful_Web_Serivces));
            p.setProperty(Transform_API, pItems.getProperty(Transform_API));
            p.setProperty(Transform_Parameters, pItems.getProperty(Transform_Parameters));
            p.setProperty(Report_Manager, pItems.getProperty(Report_Manager));
            p.setProperty(IP_Based_Authentication, pItems.getProperty(IP_Based_Authentication));
            p.setProperty(Certificate_based_Authentication, pItems.getProperty(Certificate_based_Authentication));
            p.setProperty(Secret_Token_based_Authentication, pItems.getProperty(Secret_Token_based_Authentication));
            p.setProperty(Day_And_Time_Restriction, pItems.getProperty(Day_And_Time_Restriction));
            p.setProperty(Transactions_per_day_Controller, pItems.getProperty(Transactions_per_day_Controller));
            p.setProperty(Transaction_per_second_tps_controller, pItems.getProperty(Transaction_per_second_tps_controller));
            p.setProperty(Resource_Monitoring, pItems.getProperty(Resource_Monitoring));
            p.setProperty(Billing_manager, pItems.getProperty(Billing_manager));
            p.setProperty(Clustering_between_instances, pItems.getProperty(Clustering_between_instances));
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean IsSecurityInitialized() {
        if (aeskey == null || aeskey.isEmpty() == true) {
            return false;
        } else {
            return true;
        }
    }

    public static int SetPasswords(String[] passwords) {
        return SetPasswordsInner(passwords);
    }

    public static int ReplacePasswords(String[] curpasswords, String[] newpasswords) {
        return ReplacePasswordsInner(curpasswords, newpasswords);
    }

    public static byte[] AccessDataBytes(byte[] bytesEncryptedData) {
        try {
            if (bytesEncryptedData == null) {
                return null;
            }
            byte[] raw = aes_password;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.decryptAES(aeskey1, bytesEncryptedData);
            return original;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String AccessData(String strEncryptedData) {
        try {
            if (strEncryptedData == null || strEncryptedData.isEmpty() == true) {
                return null;
            }
            byte[] hexBytes = HWSignature.hex2Byte(strEncryptedData);
            byte[] raw = aes_password;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.decryptAES(aeskey1, hexBytes);
            return new String(original);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String ProtectData(String strData) {
        try {
            if (strData == null || strData.isEmpty() == true) {
                return null;
            }
            byte[] raw = aes_password;
            if (aes_password == null) {
                Date d = new Date();
                System.out.println(d + ">>" + "Protect Data aes_password is NULL ");
                return null;
            } else {
            }
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.encryptAES(aeskey1, strData.getBytes());
            String strEncryptedData = HWSignature.asHex(original);
            return strEncryptedData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] ProtectDataBytes(byte[] bytesData) {
        try {
            if (bytesData == null) {
                return null;
            }
            byte[] raw = aes_password;
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey1 = cmObj.generateKeyAES(raw, 128);
            byte[] original = cmObj.encryptAES(aeskey1, bytesData);
            return original;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String GenerateRandomNumber() {
        Date d = new Date();
        return "" + d.getTime();
    }

    private static Properties g_LicenseProperties = null;

    public static int CheckEnforcementFor(String strType) {
        if (g_LicenseProperties == null) {
            try {
                g_LicenseProperties = LicenseDetails();
            } catch (Exception ex) {
                return -1;
            }
        }
        try {
            String status = g_LicenseProperties.getProperty(strType);
            if (status == null) {
                return -1;
            }
            if (status.compareToIgnoreCase("yes") == 0) {
                return 0;
            }
        } catch (Exception ex) {
            return -1;
        }
        return -1;
    }

    public static int GetChannelsAllowed() {
        return 2;
    }

    public static Properties mapToProperties(HashMap<String, String> map) {
        Properties p = new Properties();
        Set<Map.Entry<String, String>> set = map.entrySet();
        for (Map.Entry<String, String> entry : set) {
            p.put(entry.getKey(), entry.getValue());
        }
        return p;
    }

    public static int GetOperatorsAllowed() {
        return 50;
    }

    public static int GetResourceAllowed() {
        if (g_LicenseProperties == null) {
            try {
                g_LicenseProperties = LicenseDetails();
            } catch (Exception ex) {
                return -1;
            }
        }
        try {
            String count = g_LicenseProperties.getProperty(Resources);
            if (count == null) {
                return -1;
            }
            int iCount = Integer.parseInt(count);
            return iCount;
        } catch (Exception ex) {
            return -1;
        }
    }

    public static int GetAcccesspointsAllowed() {
        if (g_LicenseProperties == null) {
            try {
                g_LicenseProperties = LicenseDetails();
            } catch (Exception ex) {
                return -1;
            }
        }
        try {
            String count = g_LicenseProperties.getProperty(Accesspoints);
            if (count == null) {
                return -1;
            }
            int iCount = Integer.parseInt(count);
            return iCount;
        } catch (Exception ex) {
            return -1;
        }
    }

    public static int GetGroupsAllowed() {
        if (g_LicenseProperties == null) {
            try {
                g_LicenseProperties = LicenseDetails();
            } catch (Exception ex) {
                return -1;
            }
        }
        try {
            String count = g_LicenseProperties.getProperty(GroupManagement);
            if (count == null) {
                return -1;
            }
            int iCount = Integer.parseInt(count);
            return iCount;
        } catch (Exception ex) {
            return -1;
        }
    }

    public static int GetPartnersAllowed() {
        if (g_LicenseProperties == null) {
            try {
                g_LicenseProperties = LicenseDetails();
            } catch (Exception ex) {
                return -1;
            }
        }
        try {
            String count = g_LicenseProperties.getProperty(Partnermanagement);
            if (count == null) {
                return -1;
            }
            int iCount = Integer.parseInt(count);
            return iCount;
        } catch (Exception ex) {
            return -1;
        }
    }

    public static String getContents(File aFile) {
        StringBuilder contents = new StringBuilder();
        try {
            BufferedReader input = new BufferedReader(new FileReader(aFile));
            try {
                String line = null;
                while ((line = input.readLine()) != null) {
                    contents.append(line);
                }
            } finally {
                input.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return contents.toString();
    }

    public static String getContentsOfDBConf(File aFile) {
        StringBuilder contents = new StringBuilder();
        try {
            BufferedReader input = new BufferedReader(new FileReader(aFile));
            try {
                String line = "";
                while ((line = input.readLine()) != null) {
                    contents.append(line + "\n");
                }
            } finally {
                input.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return contents.toString();
    }
}
