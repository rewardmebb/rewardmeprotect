package com.mollatech.service.nucleus.crypto;

import java.io.File;
import java.util.Date;
import java.util.Properties;

public class LoadSettings {

    public static Properties g_sSettings = null;

    public static Properties g_eSettings = null;

    public static String e_strPath = null;

    public static String g_strPath = null;

    public static Properties g_templateSettings = null;

    public static String g_templatestrPath = null;

    public static Properties g_subjecttemplateSettings = null;

    public static String g_subjecttemplatestrPath = null;

    public static ChannelProfile gs_channelprofilesettting = null;

    public static Properties g_partnerRejectionSettings = null;
    public static Properties g_packageRejectionSettings = null;
    public static Properties g_promocodeRejectionSettings = null;
    public static Properties g_pdfADRejectionSettings = null;
     public static Properties g_emailADRejectionSettings = null;
    
    public static String g_partnerRejectionPath = null;
    public static String g_packageRejectionPath = null;
    public static String g_promocodeRejectionPath = null;
    public static String g_pdfADRejectionPath = null;
    public static String g_emailADRejectionPath = null;
    
    public static Properties g_emailCategorySettings = null;
    public static String g_emailCategoryPath = null;

    public static Properties g_emailSubjectSettings = null;
    public static String g_emailSubjectPath = null;

    public static Properties g_resourceOwnerRejectionSettings = null;
    public static String g_resourceOwnerRejectionPath = null;
    
    public static Properties g_productionAccessRejectionSettings = null;
    public static String g_productionAccessRejectionPath = null;
    
    public static String pdfSavePath = null;
    public static String emailAddSavePath = null;
    static {
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = System.getenv("SGHOME");
        }
        if (usrhome == null) {
            String path = sep + "root" + sep +  "TMAPIV2" + sep +  "worker";
            File f = new File(path);
            if (f.exists() && f.isDirectory()) {
                usrhome = path;
            } else {
                path = sep + "sguard" + sep + "worker";
                f = new File(path);
                if (f.exists() && f.isDirectory()) {
                    usrhome = path;
                } else {
                    path = sep + "app" + sep + "sgworker";
                    f = new File(path);
                    if (f.exists() && f.isDirectory()) {
                        usrhome = path;
                    } else {
                        path = "C:\\Users\\Ash\\Desktop\\apache-tomcat-8.0.24";
                        f = new File(path);
                        if (f.exists() && f.isDirectory()) {
                            usrhome = path;
                        } else {
                            path = "/Users/Ashu/Pictures/ashish/Hackathon";
                            f = new File(path);
                            if (f.exists() && f.isDirectory()) {
                                usrhome = path;
                            }
                        }
                    }
                }
            }
        }
        if (usrhome == null) {
            usrhome = "/Users/abhishekingle/Abhi/RewardMe/apache-tomcat-8.0.24";
        }  
        pdfSavePath = usrhome + sep +"webapps"+sep +"RewardMe"+sep;
        usrhome += sep + "rewardme-settings";
        g_strPath = usrhome + sep;
        String filepath = usrhome + sep + "dbsetting.conf";
        PropsFileUtil p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_sSettings = p.properties;
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "dbsetting setting file failed to load >> " + filepath);
        }
        g_templatestrPath = usrhome + sep + "templates";
        filepath = g_templatestrPath + sep + "templates.conf";
        p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_templateSettings = p.properties;
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "templates setting file failed to load >> " + filepath);
        }
        g_subjecttemplatestrPath = usrhome + sep + "templates";
        filepath = g_subjecttemplatestrPath + sep + "subject.templates.conf";
        p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_subjecttemplateSettings = p.properties;
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "subject templates setting file failed to load >> " + filepath);
        }

        g_partnerRejectionPath = usrhome + sep + "partnerRejectionCause.conf";
        filepath = g_partnerRejectionPath;
        p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_partnerRejectionSettings = p.properties;
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "partner rejection setting file failed to load >> " + filepath);
        }

        g_packageRejectionPath = usrhome + sep + "packageRejectionCause.conf";
        filepath = g_packageRejectionPath;
        p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_packageRejectionSettings = p.properties;
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "package rejection setting file failed to load >> " + filepath);
        }
                        
        
        
        
        g_emailCategoryPath = usrhome + sep + "EmailCategory.conf";
        filepath = g_emailCategoryPath;

        p = new PropsFileUtil();

        if (p.LoadFile(filepath) == true) {
            g_emailCategorySettings = p.properties;
//            System.out.println("dbsetting setting file loaded >>" + filepath);
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "email category setting file failed to load >> " + filepath);
        }

        g_emailSubjectPath = usrhome + sep + "EmailSubject.conf";
        filepath = g_emailSubjectPath;

        p = new PropsFileUtil();

        if (p.LoadFile(filepath) == true) {
            g_emailSubjectSettings = p.properties;
//            System.out.println("dbsetting setting file loaded >>" + filepath);
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "email category setting file failed to load >> " + filepath);
        }

        
        g_emailADRejectionPath = usrhome + sep + "emailADRejectionCause.conf";
        filepath = g_emailADRejectionPath;

        p = new PropsFileUtil();

        if (p.LoadFile(filepath) == true) {
            g_emailADRejectionSettings = p.properties;
        } else {
            Date d = new Date();
            System.out.println(d + ">>" + "email ad rejection setting file failed to load >> " + filepath);
        }
    }

    public static void LoadManually() {
        DBSettingManual();
    }

    private static void DBSettingManual() {
        String sep = System.getProperty("file.separator");        
        String filepath = g_strPath + sep + "dbsetting.conf";
        PropsFileUtil p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_sSettings = p.properties;
        } else {
            System.out.println(new Date() + ">>" + "manually loaded dbsetting setting file failed to load >> " + filepath);
        }
    }

    public static void LoadChannelProfile(ChannelProfile chprofile) {
        gs_channelprofilesettting = chprofile;
        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        if (usrhome == null) {
            usrhome = sep + "var" + sep + "axiomprotect2" + sep + "interface";
        }
        usrhome += sep + "serviceguard-settings";
        g_strPath = usrhome + sep;
        String filepath = usrhome + sep + "dbsetting.conf";
        PropsFileUtil p = new PropsFileUtil();
        if (p.LoadFile(filepath) == true) {
            g_sSettings = p.properties;
            if (gs_channelprofilesettting != null) {
                g_sSettings.setProperty("bulk.email.attachment", gs_channelprofilesettting.bulkemailattachment);
                g_sSettings.setProperty("cleanup.log", gs_channelprofilesettting.cleanuppath);
                g_sSettings.setProperty("check.user", String.valueOf(gs_channelprofilesettting.checkUser));
                g_sSettings.setProperty("user.alert", String.valueOf(gs_channelprofilesettting.alertUSer));
                g_sSettings.setProperty("user.delete", String.valueOf(gs_channelprofilesettting.deleteUser));
                g_sSettings.setProperty("user.edit", String.valueOf(gs_channelprofilesettting.editUser));
                g_sSettings.setProperty("alert.media", gs_channelprofilesettting.alertmedia);
                g_sSettings.setProperty("sw.otp.type", gs_channelprofilesettting.swotptype);
                g_sSettings.setProperty("tokensetting.load", gs_channelprofilesettting.tokensettingload);
                g_sSettings.setProperty("otp.specification", gs_channelprofilesettting.otpspecification);
                g_sSettings.setProperty("cert.specification", gs_channelprofilesettting.certspecification);
                g_sSettings.setProperty("sign.specification", gs_channelprofilesettting.signspecification);
                g_sSettings.setProperty("location.classname", gs_channelprofilesettting.locationclassname);
                g_sSettings.setProperty("user.reset", String.valueOf(gs_channelprofilesettting.resetUser));
                g_sSettings.setProperty("connector.status.check.time", String.valueOf(gs_channelprofilesettting.connectorStatus));
                g_sSettings.setProperty("audit.clean.up.duration.days", String.valueOf(gs_channelprofilesettting.cleanupdays));
            } else {
                String rssarchivepath = usrhome + sep + "rss-archive";
                String _cleanuppath = usrhome + sep + "cleanup.log";
                String _bulkattachmentpath = usrhome + sep + "uploads" + sep;
                g_sSettings.setProperty("remote.sign.archive", rssarchivepath);
                g_sSettings.setProperty("bulk.email.attachment", _bulkattachmentpath);
                g_sSettings.setProperty("cleanup.log", _cleanuppath);
                g_sSettings.setProperty("check.user", String.valueOf(1));
                g_sSettings.setProperty("user.alert", String.valueOf(1));
                g_sSettings.setProperty("user.delete", String.valueOf(true));
                g_sSettings.setProperty("user.edit", String.valueOf(true));
                g_sSettings.setProperty("alert.media", "sms");
                g_sSettings.setProperty("sw.otp.type", "simple");
                g_sSettings.setProperty("tokensetting.load", "yes");
                g_sSettings.setProperty("otp.specification", "OCRA-1\\:HOTP-SHA1-6\\:QN08");
                g_sSettings.setProperty("cert.specification", "OCRA-1\\:HOTP-SHA1-8\\:QA08");
                g_sSettings.setProperty("sign.specification", "OCRA-1\\:HOTP-SHA1-8\\:QA08");
                g_sSettings.setProperty("location.classname", "com.mollatech.internal.handler.geolocation.AxiomLocationImpl");
                g_sSettings.setProperty("user.reset", String.valueOf(1));
                g_sSettings.setProperty("connector.status.check.time", String.valueOf(300));
                g_sSettings.setProperty("audit.clean.up.duration.days", String.valueOf(180));
            }
        } else {
            System.out.println(new Date() + ">>" + "dbsetting setting file failed to Set >> " + filepath);
        }
    }
}
