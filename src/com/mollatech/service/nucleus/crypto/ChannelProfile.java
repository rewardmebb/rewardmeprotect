package com.mollatech.service.nucleus.crypto;

import java.io.Serializable;

/**
 *
 * @author Ideasventure
 */
public class ChannelProfile implements Serializable {

    private static final long serialVersionUID = 599901307498275970L;

    public static final int _CHECKUSERBYNAME = 1;

    public static final int _CHECKUSERBYEMAILPHONEUSERID = 2;

    public static final int _NOUSERALERT = 0;

    public static final int _USERALERT = 1;

    public static final String _ALERTMEDIASMS = "sms";

    public static final String _ALERTMEDIAEMAIL = "email";

    public static final String _ALERTMEDIAVOICE = "voice";

    public static final String _ALERTMEDIAUSSD = "ussd";

    public static final String _SWOTPTYPESIMPLE = "simple";

    public static final String _SWOTPTYPEMOBILE = "mobiletrust";

    public static final String tokensettingloadyes = "yes";

    public static final String tokensettingloadno = "no";

    public static final int _RESETUSERTOACTIVE = 1;

    public static final int _RESETUSERTOUNASSIGN = 0;

    public static final int CONNECTORSTATUSFIVEMINS = 300;

    public static final int CONNECTORSTATUSTENMINS = 600;

    public static final int CONNECTORSTATUSTHIRTYMINS = 1800;

    public static final int CONNECTORSTATUSONEHOUR = 3600;

    public static final int CONNECTORSTATUSTHREEHOUR = 10800;

    public static final int CLEANUPDAYSNINETY = 90;

    public static final int CLEANUPDAYSONEEIGHTY = 180;

    public static final int CLEANUPDAYSTWOSEVENTY = 270;

    public static final int CLEANUPDAYSTHREESIXTYFOUR = 364;

    public static final int CLEANUPDAYSSEVENTWENYEIGHT = 728;

    public int resetUser;

    public int connectorStatus;

    public int checkUser;

    public int alertUSer;

    public int cleanupdays;

    public boolean deleteUser;

    public boolean editUser;

    public String tokensettingload;

    public String alertmedia;

    public String swotptype;

    public String cleanuppath;

    public String remotesignarchive;

    public String bulkemailattachment;

    public String locationclassname;

    public String otpspecification;

    public String certspecification;

    public String signspecification;
}
