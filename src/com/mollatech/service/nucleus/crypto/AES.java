package com.mollatech.service.nucleus.crypto;

import com.mollatech.rewardme.mobiletrust.crypto.CryptoManager;
import javax.crypto.*;
import javax.crypto.spec.*;
import org.bouncycastle.util.encoders.Base64;

public class AES extends HWSignature {

    private static String AES_ALGO = "AES/CBC/NoPadding";

    public byte[] hexToBytes(char[] hex) {
        int length = hex.length / 2;
        byte[] raw = new byte[length];
        for (int i = 0; i < length; i++) {
            int high = Character.digit(hex[i * 2], 16);
            int low = Character.digit(hex[i * 2 + 1], 16);
            int value = (high << 4) | low;
            if (value > 127) {
                value -= 256;
            }
            raw[i] = (byte) value;
        }
        return raw;
    }

    public String PINDecrypt1(String strEncryptedData) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(HWSignature.strHDDSignature.getBytes(), 128);
            byte[] encDataBytes = hex2Byte(strEncryptedData);
            byte[] byteEncryptedData = cmObj.decryptAES(aeskey, encDataBytes);
            return new String(byteEncryptedData);
        } catch (Exception e) {
            return null;
        }
    }

    public String PINEncrypt1(String strData) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(HWSignature.strHDDSignature.getBytes(), 128);
            byte[] byteEncryptedData = cmObj.encryptAES(aeskey, strData.getBytes());
            return asHex(byteEncryptedData);
        } catch (Exception e) {
            return null;
        }
    }

    public String PINEncrypt1(String strData, String password) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte[] byteEncryptedData = cmObj.encryptAES(aeskey, strData.getBytes());
            return asHex(byteEncryptedData);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String PINDecrypt1(String strEncryptedData, String password) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte[] encDataBytes = hex2Byte(strEncryptedData);
            byte[] byteEncryptedData = cmObj.decryptAES(aeskey, encDataBytes);
            return new String(byteEncryptedData);
        } catch (Exception e) {
            return null;
        }
    }

    public String PINEncryptBytes(byte[] strData, String password) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte[] byteEncryptedData = cmObj.encryptAES(aeskey, strData);
            return asHex(byteEncryptedData);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String PINEncrypt(String strData, String password) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte[] byteEncryptedData = cmObj.encryptAES(aeskey, strData.getBytes());
            return asHex(byteEncryptedData);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public byte[] PINDecryptBytes(String strEncryptedData, String password) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte[] encDataBytes = hex2Byte(strEncryptedData);
            byte[] byteEncryptedData = cmObj.decryptAES(aeskey, encDataBytes);
            return byteEncryptedData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String PINDecrypt(String strEncryptedData, String password) {
        try {
            CryptoManager cmObj = new CryptoManager();
            byte[] aeskey = cmObj.generateKeyAES(password.getBytes(), 128);
            byte[] encDataBytes = hex2Byte(strEncryptedData);
            byte[] byteEncryptedData = cmObj.decryptAES(aeskey, encDataBytes);
            return new String(byteEncryptedData);
        } catch (Exception e) {
            return null;
        }
    }
}
