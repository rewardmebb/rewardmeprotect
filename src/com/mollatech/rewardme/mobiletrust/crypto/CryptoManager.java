package com.mollatech.rewardme.mobiletrust.crypto;

import com.mollatech.service.nucleus.crypto.AxiomProtect;
import com.mollatech.service.nucleus.crypto.OCRA;
import com.mollatech.service.nucleus.crypto.TOTP;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author vikramsareen
 */
public class CryptoManager {

    private static String AES_ALGO = "AES/CBC/PKCS5Padding";

    private static final byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    public static final int RSA = 1;

    public static final int SHA1WithRSA = 2;

    public static final int ANDROID = 1;

    public static final int IOS = 2;

    private static byte[] v = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    static {
        Provider p = Security.getProvider("BC");
        if (p == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    public static void main(String args[]) throws Exception {
        CryptoManager cmObj = new CryptoManager();
        byte[] b = cmObj.generateKeyAESForAndroid("test password".toCharArray(), 128);
        b = cmObj.generateKeyAESForAndroid("test password".toCharArray(), 128);
    }

    public String generateTokenSecret(String regcode, String deviceid) {
        return generateTokenSecretInner(regcode, deviceid);
    }

    private String generateTokenSecretInner(String regcode, String deviceid) {
        try {
            String strRegCodeSecret = deviceid;
            strRegCodeSecret += regcode;
            Date d = new Date();
            strRegCodeSecret += d.getTime();
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(strRegCodeSecret.getBytes());
            byte[] output = md.digest();
            String strHexSecret = asHex(output);
            return strHexSecret;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;
        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }
        return strbuf.toString();
    }

    public String generateOTP(String secret, int iLength, int otpDurationinSeconds) {
        return generateOTPInner(secret, iLength, otpDurationinSeconds);
    }

    private String generateOTPInner(String secret, int iLength, int otpDurationinSeconds) {
        long tOTP = new java.util.Date().getTime() / 1000;
        String strOTPLength = "";
        strOTPLength += iLength;
        TOTP totpObj = new TOTP();
        String otp = totpObj.GetOTP(secret, tOTP, strOTPLength, 0, 1, otpDurationinSeconds);
        Date d = new Date(tOTP * 1000);
        return otp;
    }

    public String generateSignatureOTP(String secret, String[] data, int iOtpDurationInSeconds, int iSOTPLength) throws Exception {
        return generateSignatureOTPInner(secret, data, iOtpDurationInSeconds, iSOTPLength);
    }

    private String generateSignatureOTPInner(String secret, String[] data, int iOtpDurationInSeconds, int iSOTPLength) throws Exception {
        String strAllData = "";
        for (int i = 0; i < data.length; i++) {
            strAllData += data[i];
        }
        String ocraSuite = "OCRA-1:TOTP-SHA1-";
        ocraSuite += iSOTPLength;
        ocraSuite += ":QN64";
        if (iOtpDurationInSeconds == 60) {
            ocraSuite += "-T1M";
        } else if (iOtpDurationInSeconds == 120) {
            ocraSuite += "-T2M";
        } else if (iOtpDurationInSeconds == 180) {
            ocraSuite += "-T3M";
        }
        String questionHex = asHex(strAllData.getBytes());
        BigInteger b = new BigInteger("0");
        java.util.Date date1 = new java.util.Date();
        b = BigInteger.valueOf(date1.getTime());
        long otptimeToCal = iOtpDurationInSeconds * 1000;
        b = b.divide(new BigInteger("" + otptimeToCal));
        String timeStamp = b.toString(16);
        OCRA ocraObj = new OCRA();
        String sotp = ocraObj.generateOCRA(ocraSuite, secret, "", questionHex, "", "", timeStamp);
        return sotp;
    }

    public AxiomRSAKeyPair generateAxiomRSAKeyPair(int iLength) throws NoSuchAlgorithmException {
        return generateAxiomRSAKeyPairInner(iLength);
    }

    public boolean verifyDataRSA(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
        return verifyDataRSAInner(data, sigBytes, publicKey, type);
    }

    public byte[] signDataRSA(byte[] data, PrivateKey privateKey, int type) {
        return signDataRSAInner(data, privateKey, type);
    }

    public byte[] encryptRSA(byte[] plainData, PublicKey pubKey) {
        return encryptRSAInner(plainData, pubKey);
    }

    public byte[] encryptRSAForAndroid(byte[] plainData, PublicKey pubKey) {
        return encryptRSAForAndroidInner(plainData, pubKey);
    }

    public byte[] decryptRSA(byte[] encrptdByte, PrivateKey privateKey) {
        return decryptRSAInner(encrptdByte, privateKey);
    }

    public byte[] decryptRSAForAndroid(byte[] encrptdByte, PrivateKey privateKey) {
        return decryptRSAForAndroidInner(encrptdByte, privateKey);
    }

    public byte[] generateKeyAES(byte[] keyStart, int iLength) {
        return generateKeyAESInner(keyStart, iLength);
    }

    public byte[] generateKeyAESForAndroid(char[] keyStart, int iLength) {
        return generateKeyAESForAndroidInner(keyStart, iLength);
    }

    public byte[] encryptAES(byte[] key, byte[] clear) {
        return encryptAESInner(key, clear);
    }

    public byte[] decryptAES(byte[] key, byte[] encrypted) {
        return decryptAESInner(key, encrypted);
    }

    private AxiomRSAKeyPair generateAxiomRSAKeyPairInner(int iLength) throws NoSuchAlgorithmException {
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(iLength);
            KeyPair keyPair = kpg.genKeyPair();
            AxiomRSAKeyPair arsakp = new AxiomRSAKeyPair();
            arsakp.visiblekey = keyPair.getPublic();
            arsakp.privatekey = keyPair.getPrivate();
            return arsakp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] encryptRSAInner(byte[] plainData, PublicKey pubKey) {
        try {
            byte[] encryptionByte = null;
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            byte[] someData = cipher.update(plainData);
            byte[] moreData = cipher.doFinal();
            byte[] encrypted = new byte[someData.length + moreData.length];
            System.arraycopy(someData, 0, encrypted, 0, someData.length);
            System.arraycopy(moreData, 0, encrypted, someData.length, moreData.length);
            encryptionByte = encrypted;
            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] encryptRSAForAndroidInner(byte[] plainData, PublicKey pubKey) {
        try {
            byte[] encryptionByte = null;
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            encryptionByte = cipher.doFinal(plainData);
            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] decryptRSAInner(byte[] encrptdByte, PrivateKey privateKey) {
        try {
            byte[] encryptionByte = null;
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] someDecrypted = cipher.update(encrptdByte);
            byte[] moreDecrypted = cipher.doFinal();
            byte[] decrypted = new byte[someDecrypted.length + moreDecrypted.length];
            System.arraycopy(someDecrypted, 0, decrypted, 0, someDecrypted.length);
            System.arraycopy(moreDecrypted, 0, decrypted, someDecrypted.length, moreDecrypted.length);
            encryptionByte = decrypted;
            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] decryptRSAForAndroidInner(byte[] encrptdByte, PrivateKey privateKey) {
        try {
            byte[] encryptionByte = null;
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            encryptionByte = cipher.doFinal(encrptdByte);
            return encryptionByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] signDataRSAInner(byte[] data, PrivateKey privateKey, int type) {
        try {
            Signature signature = null;
            if (type == RSA) {
                signature = Signature.getInstance("RSA", "BC");
            } else {
                signature = Signature.getInstance("SHA1WithRSA");
            }
            signature.initSign(privateKey);
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(data);
            byte[] hashedData = md.digest();
            signature.update(hashedData);
            byte[] sig = signature.sign();
            return sig;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private boolean verifyDataRSAInner(byte[] data, byte[] sigBytes, PublicKey publicKey, int type) {
        try {
            Signature signature = null;
            if (type == RSA) {
                signature = Signature.getInstance("RSA", "BC");
            } else {
                signature = Signature.getInstance("SHA1WithRSA");
            }
            signature.initVerify(publicKey);
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(data);
            byte[] hashedData = md.digest();
            signature.update(hashedData);
            return signature.verify(sigBytes);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private byte[] generateKeyAESInner(byte[] keyStart, int iLength) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(keyStart);
            kgen.init(iLength, sr);
            SecretKey skey = kgen.generateKey();
            byte[] key = skey.getEncoded();
            return key;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] generateKeyAESForAndroidInner(char[] password, int iLength) {
        try {
            int iterationCount = 1000;
            int keyLength = iLength;
            KeySpec keySpec = new PBEKeySpec(password, iv, iterationCount, keyLength);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();
            SecretKey skey = new SecretKeySpec(keyBytes, "AES");
            byte[] key = skey.getEncoded();
            return key;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] encryptAESInner(byte[] key, byte[] clear) {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance(AES_ALGO);
            IvParameterSpec ivspec = new IvParameterSpec(iv);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);
            byte[] encrypted = cipher.doFinal(clear);
            return encrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] decryptAESInner(byte[] key, byte[] encrypted) {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance(AES_ALGO);
            IvParameterSpec ivspec = new IvParameterSpec(iv);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
            byte[] decrypted = cipher.doFinal(encrypted);
            return decrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String ConsumeMobileTrust(String jsonData, String reciverPrivateKey, boolean bVerifyNeeded) {
        return ConsumeMobileTrustInner(jsonData, reciverPrivateKey, bVerifyNeeded);
    }

    private String ConsumeMobileTrustInner(String jsonData, String reciverPrivateKey, boolean bVerifyNeeded) {
        try {
            JSONObject jsonObj = new JSONObject(jsonData);
            String _data = jsonObj.getString("_data");
            String _key = jsonObj.getString("_key");
            String _signature = jsonObj.getString("_signature");
            String senderPublicKey = jsonObj.getString("_visiblekey");
            String device = jsonObj.getString("_deviceType");
            int devicetype = 1;
            if (device.equals("ANDROID")) {
                devicetype = ANDROID;
            } else if (device.equals("IOS")) {
                devicetype = IOS;
            }
            CryptoManager cm = new CryptoManager();
            byte[] encdataBytes = Base64.decode(_data);
            byte[] encKeyBytes = Base64.decode(_key);
            byte[] singatureBytes = Base64.decode(_signature);
            byte[] reciverPrivateKeyBytes = Base64.decode(reciverPrivateKey);
            byte[] privkey = AxiomProtect.AccessDataBytes(reciverPrivateKeyBytes);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
            PrivateKey pk = kf.generatePrivate(ks);
            byte[] aeskey = null;
            if (devicetype == ANDROID) {
                aeskey = cm.decryptRSAForAndroid(encKeyBytes, pk);
            } else if (devicetype == IOS) {
                aeskey = cm.decryptRSA(encKeyBytes, pk);
            }
            byte[] plaindataBytes = cm.decryptAES(aeskey, encdataBytes);
            if (bVerifyNeeded == true) {
                if (devicetype == ANDROID) {
                    X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(senderPublicKey));
                    KeyFactory fact = KeyFactory.getInstance("RSA");
                    PublicKey publicKey = fact.generatePublic(spec);
                    bVerifyNeeded = cm.verifyDataRSA(plaindataBytes, singatureBytes, publicKey, SHA1WithRSA);
                } else if (devicetype == IOS) {
                    PublicKey publicKey = this.getPublicKey(senderPublicKey);
                    bVerifyNeeded = cm.verifyDataRSA(plaindataBytes, singatureBytes, publicKey, RSA);
                }
            } else {
                return new String(plaindataBytes);
            }
            if (bVerifyNeeded == true) {
                return new String(plaindataBytes);
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
        return null;
    }

    public String EnforceMobileTrust(JSONObject json, String password, String recieverPublicKey, String senderPrivateKey, String senderPublicKey, int deviceType) {
        return EnforceMobileTrustInner(json, password, recieverPublicKey, senderPrivateKey, senderPublicKey, deviceType);
    }

    private String EnforceMobileTrustInner(JSONObject json, String password, String recieverPublicKey, String senderPrivateKey, String senderPublicKey, int deviceType) {
        try {
            CryptoManager cm = new CryptoManager();
            byte[] privkey = AxiomProtect.AccessDataBytes(Base64.decode(senderPrivateKey));
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
            PrivateKey pk = kf.generatePrivate(ks);
            byte[] signatureBytes = null;
            if (deviceType == IOS) {
                signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, RSA);
            } else if (deviceType == ANDROID) {
                signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, SHA1WithRSA);
            }
            byte[] aesKey = null;
            if (deviceType == IOS) {
                aesKey = cm.generateKeyAES(password.getBytes(), 128);
            } else if (deviceType == ANDROID) {
                aesKey = cm.generateKeyAESForAndroid(password.toCharArray(), 128);
            }
            byte[] encryptedAESJSONDATA = cm.encryptAES(aesKey, json.toString().getBytes());
            X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(recieverPublicKey));
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PublicKey publicKey = fact.generatePublic(spec);
            byte[] encryptedAESKEY = null;
            if (deviceType == IOS) {
                encryptedAESKEY = cm.encryptRSA(aesKey, publicKey);
            } else if (deviceType == ANDROID) {
                encryptedAESKEY = cm.encryptRSAForAndroid(aesKey, publicKey);
            }
            String singature = new String(Base64.encode(signatureBytes));
            String data = new String(Base64.encode(encryptedAESJSONDATA));
            String key = new String(Base64.encode(encryptedAESKEY));
            Properties p = new Properties();
            p.setProperty("_signature", singature);
            p.setProperty("_data", data);
            p.setProperty("_key", key);
            p.setProperty("_visibleKey", senderPublicKey);
            UtilityImpl utils = new UtilityImpl();
            JSONObject jsonObjToProtect = utils.GenerateJSON(p);
            return jsonObjToProtect.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
        return null;
    }

    public String MobileTrustSignOnly(JSONObject json, String senderPrivateKey, int devicetype) {
        return MobileTrustSignOnlyInner(json, senderPrivateKey, devicetype);
    }

    private String MobileTrustSignOnlyInner(JSONObject json, String senderPrivateKey, int devicetype) {
        try {
            CryptoManager cm = new CryptoManager();
            byte[] privkey = AxiomProtect.AccessDataBytes(Base64.decode(senderPrivateKey));
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(privkey);
            PrivateKey pk = kf.generatePrivate(ks);
            byte[] signatureBytes = null;
            if (devicetype == ANDROID) {
                signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, SHA1WithRSA);
            } else if (devicetype == IOS) {
                signatureBytes = cm.signDataRSA(json.toString().getBytes(), pk, RSA);
            }
            String singature = new String(Base64.encode(signatureBytes));
            String data = new String(Base64.encode(json.toString().getBytes()));
            Properties p = new Properties();
            p.setProperty("_signature", singature);
            p.setProperty("_data", data);
            UtilityImpl utils = new UtilityImpl();
            JSONObject jsonObjToProtect = utils.GenerateJSON(p);
            utils = null;
            return jsonObjToProtect.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
        return null;
    }

    private PublicKey getPublicKey(String publicKey) {
        try {
            String pemKey = "-----BEGIN RSA PUBLIC KEY-----\n" + publicKey + "\n" + "-----END RSA PUBLIC KEY-----\n";
            PEMReader pemReader = new PEMReader(new StringReader(pemKey));
            RSAPublicKey rsaPubKey = (RSAPublicKey) pemReader.readObject();
            PublicKey pubk = rsaPubKey;
            return pubk;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String decryptSecreteAES128(byte[] secrete, String filename) throws NoSuchAlgorithmException {
        File f = new File(filename);
        try {
            byte[] key = this.read(f);
            byte[] secrte = decryptAES(key, secrete);
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(secrte);
            byte[] output = md.digest();
            byte[] strPS = Base64.decode("MDkMYHMLyFkWzP20t+tcI5mSyfk=");
        } catch (IOException ex) {
            Logger.getLogger(CryptoManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String decryptSecreteAES256(byte[] secrete, String filename) {
        File f = new File(filename);
        try {
            byte[] key = this.read(f);
            byte[] secrte = decryptAES(key, secrete);
            byte[] sec = Base64.decode("MDkMYHMLyFkWzP20t+tcI5mSyfk=");
        } catch (IOException ex) {
            Logger.getLogger(CryptoManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public byte[] read(File file) throws IOException {
        byte[] buffer = new byte[(int) file.length()];
        InputStream ios = null;
        try {
            ios = new FileInputStream(file);
            if (ios.read(buffer) == -1) {
                throw new IOException("EOF reached while trying to read the whole file");
            }
        } finally {
            try {
                if (ios != null) {
                    ios.close();
                }
            } catch (IOException e) {
            }
        }
        return buffer;
    }

    public byte[] DecryptAES_CBC(byte[] data, byte[] key) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        IvParameterSpec ivspec = new IvParameterSpec(iv);
        Cipher cipher = Cipher.getInstance(AES_ALGO);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
        return cipher.doFinal(data);
    }
}
