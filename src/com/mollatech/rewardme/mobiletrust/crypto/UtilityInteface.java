package com.mollatech.rewardme.mobiletrust.crypto;

import java.util.Properties;
import org.json.JSONObject;

public interface UtilityInteface {

    public JSONObject GenerateJSON(Properties p);

    public byte[] EncryptData(int type, Object key, byte[] data);

    public byte[] DecryptData(int type, Object key, byte[] encdata);

    public byte[] SignData(byte[] data, Object key);

    public int VerifyData(byte[] data, byte[] signature, Object key);
}
