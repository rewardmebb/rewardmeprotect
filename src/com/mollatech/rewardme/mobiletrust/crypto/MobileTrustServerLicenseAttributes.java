package com.mollatech.rewardme.mobiletrust.crypto;

/**
 *
 * @author vikramsareen
 */
public class MobileTrustServerLicenseAttributes {

    public static final int BASE = 1;

    public static final int PLUS = 2;

    public String productverison;

    public long licenseexpiresOn;

    public int licensetype;

    public String fetchVisibleIPURL = null;

    public String fetchGeoURL = null;
}
