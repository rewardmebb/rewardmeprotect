package com.mollatech.rewardme.mobiletrust.crypto;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 *
 * @author vikramsareen
 */
public class AxiomRSAKeyPair {

    public PublicKey visiblekey;

    public PrivateKey privatekey;
}
